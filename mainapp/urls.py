from django.urls import path
from .views import list_pages, detail_page

urlpatterns = [
    path('pages/', list_pages, name='list_pages'),
    path('pages/<int:pk>/', detail_page, name='detail_page'),
]
