from rest_framework import serializers
from .models import Page


class PageListSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Page
        fields = ('url', 'title')
        extra_kwargs = {
            'url': {'view_name': 'detail_page'},
        }


class PageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Page
        fields = ['title']
