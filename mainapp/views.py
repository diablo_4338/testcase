from django.shortcuts import get_object_or_404
from rest_framework.decorators import api_view
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from .models import Page
from .serializers import PageListSerializer, PageSerializer
from content.views import get_content


@api_view(['GET'])
def list_pages(request):
    queryset = Page.objects.all()
    paginator = PageNumberPagination()
    paginator.page_size = 2
    paginated_queryset = paginator.paginate_queryset(queryset, request)
    serializer = PageListSerializer(paginated_queryset, many=True, context={'request': request})
    return paginator.get_paginated_response(serializer.data)


@api_view(['GET'])
def detail_page(request, pk):
    page_obj = get_object_or_404(Page, pk=pk)
    page_serializer = PageSerializer(page_obj)
    content = get_content(pk)
    result = page_serializer.data
    result.update(content)
    return Response(result)
