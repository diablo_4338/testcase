from django.db import models
from django.contrib import admin


# Create your models here.
class PageAdmin(admin.ModelAdmin):
    search_fields = ('title',)


class Page(models.Model):
    title = models.CharField(name='title', max_length=50, unique=True)
    objects = models.Manager()

    class Meta:
        ordering = ['id']

    def __str__(self):
        return self.title
