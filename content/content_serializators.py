from .serializers import VideoSerializer, TextSerializer, AudioSerializer
from .models import Video, Audio, Text

content_types = {'video': {'model': Video,
                           'serializer': VideoSerializer,
                           },
                 'audio': {'model': Audio,
                           'serializer': AudioSerializer
                           },
                 'text': {'model': Text,
                          'serializer': TextSerializer
                          },
                 }
