from rest_framework import serializers
from .models import Video, Text, Audio


class TextSerializer(serializers.ModelSerializer):
    class Meta:
        model = Text
        fields = ['title', 'text']


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Video
        fields = ['title', 'link', 'subtitles']


class AudioSerializer(serializers.ModelSerializer):
    class Meta:
        model = Audio
        fields = ['title', 'bitrate']
