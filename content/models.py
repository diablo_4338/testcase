from django.db import models
from mainapp.models import Page
from django.contrib import admin


class ContentAdmin(admin.ModelAdmin):
    search_fields = ['title']


# Create your models here.
# Абстрактный класс контента
class ContentAbstarct(models.Model):
    title = models.CharField(name='title', max_length=20, unique=True)
    counter = models.IntegerField(name='counter', default=0)
    page = models.ForeignKey(Page, on_delete=models.SET_NULL, blank=True, null=True)
    objects = models.Manager()

    class Meta:
        abstract = True
        ordering = ['-counter']

    def __str__(self):
        return self.title

    def implement_counter(self):
        self.counter += 1
        self.save()


class Video(ContentAbstarct):
    video_link = models.CharField(name='link', max_length=100, default=None)
    subtitles_link = models.CharField(name='subtitles', max_length=100, default=None)


class Audio(ContentAbstarct):
    bitrate = models.IntegerField(name='bitrate', blank=False, null=False, default=0)


class Text(ContentAbstarct):
    text = models.TextField(name='text', blank=True)
