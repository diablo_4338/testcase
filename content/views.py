from .content_serializators import content_types
from .tasks import implement_counter


# Create your views here.
def get_content(pk):
    result = {}
    for content in content_types:
        model = content_types[content].get('model')
        serializer = content_types[content].get('serializer')
        if not model or not serializer:
            raise Exception('Неверно описаны типы контента и сериализаторов')
        queryset = model.objects.all().filter(page=pk)
        if len(queryset) == 0:
            result.update({content: {}})
            continue
        data = serializer(queryset, many=True).data
        result.update({content: data})

        params = {'type': content, 'ids': [i.id for i in queryset]}
        implement_counter.delay(params)

    result = {'content': result}
    return result
