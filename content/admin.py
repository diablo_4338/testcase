from django.contrib import admin
from .models import Audio, Video, Text, ContentAdmin


# Register your models here.
# Функция для регистрации моделей в админке
def register_model(models):
    if not isinstance(models, list):
        raise Exception('Ошибка при регистрации модели контента')
    for model in models:
        admin.site.register(model, ContentAdmin)


register_model([Audio, Video, Text])
