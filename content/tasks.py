from __future__ import absolute_import, unicode_literals
from testcase.celery import app
from django.db import transaction
from django.shortcuts import get_object_or_404
from .content_serializators import content_types


@app.task
def implement_counter(queryset):
    model = content_types[queryset['type']]['model']
    for id_ in queryset['ids']:
        with transaction.atomic():
            obj = get_object_or_404(model.objects.select_for_update(), pk=id_)
            obj.implement_counter()
