Django==2.2.1
celery[redis]==4.3.0
djangorestframework==3.9.3
psycopg2-binary==2.8.2
redis==3.2.1